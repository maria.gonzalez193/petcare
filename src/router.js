import { createRouter, createWebHistory} from "vue-router";

import PrincipalUsuario from './pages/PrincipalUsuario.vue';
import ListaMascotas from './pages/mascotas/ListaMascotas.vue';
import DetalleMascota from './pages/mascotas/DetalleMascota.vue';
import RegistroMascota from './pages/mascotas/RegistroMascota.vue';
import AdopcionMascota from './pages/mascotas/AdopcionMascota.vue';
import UserAuth from './pages/auth/UserAuth.vue';
import PanelUsuario from './pages/usuarios/PanelUsuario.vue';
import PanelAdmin from './pages/admin/PanelAdmin.vue';
import ListaPosts from './pages/posts/ListaPosts.vue';
import RegistroCita from './pages/citas/RegistroCita.vue';
import DetalleCita from './pages/citas/DetalleCita.vue';
import RegistroPerfil from './pages/usuarios/RegistroPerfil.vue';

const router= createRouter({
    history: createWebHistory(),
    routes:[
        {path: '/', redirect: '/principal'},
        {path: '/principal', component:PrincipalUsuario},
        {path: '/mascotas', component:ListaMascotas},
        {path: '/mascotas/:id', component:DetalleMascota, props:true, children:[
            {path:'adopcion', component:AdopcionMascota}
        ]},
        {path: '/registro_mascota', component:RegistroMascota},
        {path: '/registro', component:UserAuth},
        {path: '/registro', component:UserAuth},
        {path: '/panel_usuario', component:PanelUsuario},
        {path: '/panel_usuario/:id', component:DetalleCita, props:true},
        {path: '/registro_perfil', component:RegistroPerfil},
        {path: '/registro_cita', component:RegistroCita},
        {path: '/panel_admin', component:PanelAdmin},
        {path: '/panel_admin/:id', component:DetalleCita, props:true},
        {path: '/posts', component:ListaPosts},
        {path: '/:noExiste(.*)', component:null}
    ]
});

export default router;