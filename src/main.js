import { createApp } from 'vue'

import router from './router.js';
import App from './App.vue'
import store from './store'
import BaseCard from './components/UI/BaseCard.vue';
import BaseButton from './components/UI/BaseButton.vue';
import BaseSpinner from './components/UI/BaseSpinner.vue';
import Vue3EasyDataTable from 'vue3-easy-data-table';
import 'vue3-easy-data-table/dist/style.css';

const app=createApp(App);

app.component('base-card',BaseCard);
app.component('base-button',BaseButton);
app.component('base-spinner',BaseSpinner);
app.component('EasyDataTable', Vue3EasyDataTable);

app.use(router);
app.use(store);

app.mount('#app');
