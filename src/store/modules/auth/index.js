import mutations from "./mutations.js";
import actions from "./actions.js";
import getters from "./getters.js";

export default{
    namespaced:true,
    state(){
        return{
            usuarios:[
                {
                    id: 'u1',
                    token:'u1',
                    email: 'maria@gmail.com',
                    contrasena: 'checha15'
                },
                {
                    id: 'u2',
                    token:'u1',
                    email: 'mario@gmail.com',
                    contrasena: 'checha16'
                }
            ]
        };

    },
    mutations,
    actions,
    getters
};