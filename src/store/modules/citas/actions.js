export default{
    async registrarCita(context, data){
        const citaData={
            nombre: data.nom,
            hora: data.hora,
            dia: data.dia,
            estatus: data.estat,
        };
        const respuesta = await fetch('https://adoptme-377cb-default-rtdb.firebaseio.com/citas.json',{
            method: 'POST',
            body: JSON.stringify(citaData)    
        });

        // const respuestaData= await respuesta.json();

        if(!respuesta.ok){
            //error
        }

        context.commit('registrarCita', citaData);
    },
    async cargarCitas(context){
        const respuesta= await fetch('https://adoptme-377cb-default-rtdb.firebaseio.com/citas.json');
        const respuestaData = await respuesta.json();
        if(!respuesta.ok){
            //error
        }

        const citas=[];
        for(const key in respuestaData){
            const cita={
                id: key,
                nombre: respuestaData[key].nombre,
                hora: respuestaData[key].hora,
                dia: respuestaData[key].dia,
                estatus: respuestaData[key].estatus,
            };
            citas.push(cita);
        }
        context.commit('fijarCitas', citas);
    }
};