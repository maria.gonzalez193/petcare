export default{
    citas(state){
        return state.citas;
    },
    existenCitas(state){
        return state.citas && state.citas.length > 0;
    }
};