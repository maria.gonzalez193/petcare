export default{
    async registrarMascota(context, data){
        const mascotaData={
            nombre: data.nom,
            sexo: data.sexo,
            edad: data.edad,
            especie: data.esp,
            pelaje: data.pelo,
            descripcion: data.desc
        };
        const respuesta = await fetch('https://adoptme-377cb-default-rtdb.firebaseio.com/mascotas.json',{
            method: 'POST',
            body: JSON.stringify(mascotaData)    
        });

        // const respuestaData= await respuesta.json();

        if(!respuesta.ok){
            //error
        }

        context.commit('registrarMascota', mascotaData);
    },
    async cargarMascotas(context){
        const respuesta= await fetch('https://adoptme-377cb-default-rtdb.firebaseio.com/mascotas.json');
        const respuestaData = await respuesta.json();
        if(!respuesta.ok){
            //error
        }

        const mascotas=[];
        for(const key in respuestaData){
            const mascota={
                id: key,
                nombre: respuestaData[key].nombre,
                sexo: respuestaData[key].sexo,
                edad: respuestaData[key].edad,
                especie: respuestaData[key].especie,
                pelaje: respuestaData[key].pelaje,
                descripcion: respuestaData[key].descripcion
            };
            mascotas.push(mascota);
        }
        context.commit('fijarMascotas', mascotas);
    },
    // async borrarMascota(context,mascota){
    //     const id = mascota['id']
    //     const raw =await fetch(`https://adoptme-377cb-default-rtdb.firebaseio.com/mascotas/${id}.json`,{
    //         method: 'DELETE',
    //         body: JSON.stringify({'id':id})
    //     });
    //     const content = await raw.json();
    //     commit('',content)
    // }
};