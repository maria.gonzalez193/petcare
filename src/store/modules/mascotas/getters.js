export default{
    mascotas:(state)=> state.mascotas,
    existenMascotas(state){
        return state.mascotas && state.mascotas.length > 0;
    }
};