import {createStore} from 'vuex';

import mascotasModule from './modules/mascotas/index.js';
import authModule from './modules/auth/index.js';
import citasModule from './modules/citas/index.js';

const store=createStore({
    modules:{
        mascotas:mascotasModule,
        auth:authModule,
        citas:citasModule
    }
});

export default store;